import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css'],
})
export class ContadorComponent implements OnInit {
  value: number = 0;
  warning: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  add() {
    if (this.value === 50) {
      this.warning = true;
    } else {
      this.value = this.value + 10;
      this.warning = false;
    }
  }

  subtract() {
    if (this.value === 0) {
      this.warning = true;

    } else {
      this.value = this.value - 10;
      this.warning = false;
    }
  }
}
